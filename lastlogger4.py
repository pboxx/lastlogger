import os, sys, time, datetime

b = os.popen("lastlog",'r',0)
b.next()

d={}

for l in b:
    ls = l.split()
    
    if '**Never logged in**' in l:
        d[ls[0]]= 0
    else:  
        date = ' '.join(ls[-5:-2] + [ls[-1]])
        t = time.strptime(date, "%b %d %H:%M:%S %Y")
        d[ls[0]]= int(time.mktime(t))
