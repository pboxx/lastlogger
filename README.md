The Last Logger
===============

Dependencies
------------

* [pyutmp](http://software.clapper.org/pyutmp/) ([package](https://packages.debian.org/stable/python-utmp)).

Munin integration
-----------------

Make sure to [setup Munin with TLS](http://munin-monitoring.org/wiki/MuninConfigurationNetworkTLS).
